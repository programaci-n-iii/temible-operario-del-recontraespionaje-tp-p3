package main;

import java.util.ArrayList;
import java.util.List;
import grafos.Arista;
import grafos.BFS;
import grafos.GrafoConPeso;
import grafos.Prim;

public class EncuentroDeEspias {

	private GrafoConPeso _grafo;
	private GrafoConPeso _grafoAGM;
	private List<String> _nombres;
	// Cada vertice del grafo con indice i le corresponde el nombre nombres.get(j),
	// con i == j.

	public EncuentroDeEspias(int n) {
		_grafo = new GrafoConPeso(n);
		_grafoAGM = new GrafoConPeso(n);
		_nombres = new ArrayList<>();
	}

	public void agregarEspia(String espia) {
		if (!_nombres.contains(espia) && !espia.isEmpty()) {
			_nombres.add(espia);
		}
	}

	public void agregarParesEspias(String espia1, String espia2, double probabilidad) {
		_grafo.agregarArista(_nombres.indexOf(espia1), _nombres.indexOf(espia2), probabilidad);
	}

	public int cantidad() {
		return _nombres.size();
	}

	public void minimizarEncuentroRiesgoso() {
		Prim prim = new Prim(_grafo);
		_grafoAGM = prim.getAGM();
	}

	public List<String> getNombres() {
		return _nombres;
	}

	public GrafoConPeso getGrafo() {
		return new GrafoConPeso(_grafo);
	}

	public String mejorEncuentroEspias() {
		StringBuilder st = new StringBuilder();
		for (Arista arista : _grafoAGM.getAristas()) {
			st.append("Espias ");
			st.append(_nombres.get(arista.getV1()));
			st.append(" y ");
			st.append(_nombres.get(arista.getV2()));
			st.append(" con probabilidad ");
			st.append(arista.getPeso());
			st.append(" de que los intercepten.");
			st.append("\n");
		}
		return st.toString();
	}

	public String encuentroEspiasBFS() {
		StringBuilder st = new StringBuilder();
		List<Arista> bfs = BFS.recorridoBFS(_grafoAGM);
		for (Arista arista : bfs) {
			if (arista != null)
				st.append("Espias ");
			st.append(_nombres.get(arista.getV1()));
			st.append(" y ");
			st.append(_nombres.get(arista.getV2()));
			st.append(" con probabilidad ");
			st.append(arista.getPeso());
			st.append(" de que los intercepten.");
			st.append("\n");
		}
		return st.toString();
	}
}

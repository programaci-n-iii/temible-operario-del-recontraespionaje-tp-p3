package main;

import java.awt.EventQueue;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;
import com.formdev.flatlaf.intellijthemes.FlatCarbonIJTheme;
import paneles.PanelMenu;

public class MainForm {

	private JFrame frame;

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainForm window = new MainForm();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public MainForm() {
		FlatCarbonIJTheme.setup();
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setSize(640, 480);
		frame.setResizable(false);
		frame.setLocationRelativeTo(null);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		JLabel textoTitulo = new JLabel("Temible operario del recontraespionaje");
		textoTitulo.setFont(new Font("Dialog", Font.BOLD, 25));
		textoTitulo.setBounds(63, 11, 483, 36);
		frame.getContentPane().add(textoTitulo);

		PanelMenu panelInicio = new PanelMenu(frame);
		frame.getContentPane().add(panelInicio);
	}
}

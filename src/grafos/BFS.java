package grafos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class BFS {

	private static List<Integer> L;
	private static boolean[] marcados;

	public static boolean esConexo(Grafo g) {
		if (g == null)
			throw new IllegalArgumentException("El grafo no puede ser nulo!");
		if (g.tamano() == 0)
			return true;
		return alcanzables(g, 0).size() == g.tamano();
	}

	public static Set<Integer> alcanzables(Grafo g, int origen) {
		Set<Integer> ret = new HashSet<Integer>();

		inicializarBusqueda(g, origen);

		while (L.size() > 0) {
			int i = seleccionarYMarcarVertice(ret);
			agregarVecinosNoMarcados(g, i);
			removerSeleccionado();
		}

		return ret;
	}

	public static List<Arista> recorridoBFS(GrafoConPeso grafo) {
		List<Arista> bfs = new ArrayList<>();

		inicializarBusqueda(grafo, 0);

		while (L.size() > 0) {
			int i = seleccionarYMarcarVertice();
			agregarAristas(grafo, i, bfs);
			agregarVecinosNoMarcados(grafo, i);
			removerSeleccionado();
		}
		return bfs;
	}

	private static void removerSeleccionado() {
		L.remove(0);
	}

	private static int seleccionarYMarcarVertice(Set<Integer> ret) {
		int i = L.get(0);
		marcados[i] = true;
		ret.add(i);
		return i;
	}

	private static int seleccionarYMarcarVertice() {
		int i = L.get(0);
		marcados[i] = true;
		return i;
	}

	private static void agregarVecinosNoMarcados(Grafo g, int i) {
		for (int vecino : g.vecinos(i)) {
			if (!marcados[vecino] && !L.contains(vecino))
				L.add(vecino);
		}
	}

	private static void agregarAristas(GrafoConPeso g, int i, List<Arista> bfs) {
		for (int vecino : g.vecinos(i)) {
			if (!marcados[vecino] && !L.contains(vecino)) {
				bfs.add(g.getArista(i, vecino));
			}
		}
	}

	private static void inicializarBusqueda(Grafo g, int origen) {
		L = new LinkedList<Integer>();
		L.add(origen);
		marcados = new boolean[g.tamano()];
	}

}
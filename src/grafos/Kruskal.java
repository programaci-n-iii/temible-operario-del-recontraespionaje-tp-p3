package grafos;

import java.util.Set;

public class Kruskal {

	private GrafoConPeso _AGM;
	private GrafoConPeso _grafoOriginal;
	private Set<Arista> _aristasComplemento;
	private UnionFind _componentes;

	public Kruskal(GrafoConPeso grafo) {
		_grafoOriginal = grafo;
		_aristasComplemento = _grafoOriginal.getAristas();
		_AGM = new GrafoConPeso(grafo.tamano());
		_componentes = new UnionFind(grafo);
		generarAGM();
	}

	private void generarAGM() {
		if (!BFS.esConexo(_grafoOriginal))
			throw new IllegalArgumentException("No existe un AGM de un grafo disconexo.");

		Integer vertice = 1;

		while (vertice < _grafoOriginal.tamano()) {
			Arista e = aristaMinimaSinFormarCiclo();

			if (e != null)
				agregarArista(e);

			vertice++;
		}
	}

	private Arista aristaMinimaSinFormarCiclo() {
		Arista aristaMinima = null;
		Double pesoMinimo = Double.MAX_VALUE;

		for (Arista arista : _aristasComplemento) {
			if (arista.getPeso() < pesoMinimo && !formaCiclo(arista)) {
				pesoMinimo = arista.getPeso();
				aristaMinima = new Arista(arista.getV1(), arista.getV2(), pesoMinimo);
			}
		}
		_aristasComplemento.remove(aristaMinima);

		return aristaMinima;
	}

	private boolean formaCiclo(Arista e) {
		return _componentes.find(e.getV1(), e.getV2());
	}

	// Al ser un arbol, si se agrega una arista se forma un ciclo
	private void agregarArista(Arista e) {
		_AGM.agregarArista(e.getV1(), e.getV2(), e.getPeso());
		_componentes.union(e.getV1(), e.getV2());
	}

	public GrafoConPeso getAGM() {
		return new GrafoConPeso(_AGM);
	}
}
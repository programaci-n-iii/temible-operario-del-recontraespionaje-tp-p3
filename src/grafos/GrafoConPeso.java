package grafos;

import java.util.Set;
import java.util.TreeSet;

public class GrafoConPeso extends Grafo {

	private Set<Arista> _aristas;

	public GrafoConPeso(int n) {
		super(n);
		_aristas = new TreeSet<>();
	}

	public GrafoConPeso(GrafoConPeso g) {
		super(g.tamano());
		_aristas = g.getAristas();
		for (Arista e : _aristas) {
			super.agregarArista(e.getV1(), e.getV2());
		}
	}

	public void agregarArista(int i, int j, double peso) {
		super.agregarArista(i, j);
		_aristas.add(new Arista(i, j, peso));
	}

	public Set<Arista> getAristas() {
		return new TreeSet<>(_aristas);
	}

	public Arista getArista(int i, int j) {
		for (Arista e : _aristas)
			if (e.getV1() == i && e.getV2() == j || e.getV1() == j && e.getV2() == i)
				return e;

		return null;
	}

	@Override
	public String toString() {
		StringBuilder st = new StringBuilder();
		for (Arista arista : _aristas) {
			st.append(arista);
			st.append("\n");
		}
		return st.toString();
	}
}
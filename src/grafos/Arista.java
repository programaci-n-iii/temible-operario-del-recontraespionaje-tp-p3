package grafos;

public class Arista implements Comparable<Arista> {

	private int _vertice1;
	private int _vertice2;
	private double _peso;

	public Arista(int vertice1, int vertice2, double peso) {
		_vertice1 = vertice1;
		_vertice2 = vertice2;
		_peso = peso;
	}

	public int getV1() {
		return _vertice1;
	}

	public int getV2() {
		return _vertice2;
	}

	public double getPeso() {
		return _peso;
	}

	@Override
	public int compareTo(Arista objArista) {
		if (_vertice1 == objArista._vertice1 && _vertice2 == objArista._vertice2
				|| _vertice1 == objArista._vertice2 && _vertice2 == objArista._vertice1) {
			return 0;
		}
		return -1;
	}

	@Override
	public String toString() {
		StringBuilder st = new StringBuilder();
		st.append("arista ");
		st.append(_vertice1);
		st.append(" - ");
		st.append(_vertice2);
		st.append(" con peso ");
		st.append(_peso);
		return st.toString();
	}

}

package grafos;

import java.util.ArrayList;
import java.util.TreeSet;

public class Grafo {

	private ArrayList<TreeSet<Integer>> _vecinos;

	public Grafo(int n) {
		_vecinos = new ArrayList<TreeSet<Integer>>();
		for (int i = 0; i < n; ++i)
			_vecinos.add(new TreeSet<>());
	}

	public void agregarArista(int i, int j) {
		verificarVertices(i, j);
		_vecinos.get(i).add(j);
		_vecinos.get(j).add(i);
	}

	public void eliminarArista(int i, int j) {
		verificarVertices(i, j);
		_vecinos.get(i).remove(j);
		_vecinos.get(j).remove(i);
	}

	public boolean existeArista(int i, int j) {
		verificarVertices(i, j);
		return _vecinos.get(i).contains(j);
	}

	public int tamano() {
		return _vecinos.size();
	}

	public TreeSet<Integer> vecinos(int i) {
		verificarVertice(i);
		for (int j = 0; j < _vecinos.size(); j++) {
			if (i == j) {
				return _vecinos.get(i);
			}
		}
		return null;
	}

	public void agregarVertice(TreeSet<Integer> vecinosNuevoVertice) {
		_vecinos.add(vecinosNuevoVertice);
	}

	public void eliminarVertice(int v) {
		for (TreeSet<Integer> vecinos : _vecinos) {
			vecinos.remove(v);
		}
		_vecinos.remove(v);
	}

	private void verificarVertices(int i, int j) {
		verificarVertice(i);
		verificarVertice(j);
		verificarDistintos(i, j);
	}

	private void verificarVertice(int i) {
		if (i < 0)
			throw new IllegalArgumentException("El vertice no puede ser negativo: " + i);
		if (i >= _vecinos.size())
			throw new IllegalArgumentException("Los vertices deben estar entre 0 y |V|-1: " + i);
	}

	private void verificarDistintos(int i, int j) {
		if (i == j)
			throw new IllegalArgumentException("No se permiten loops: (" + i + ", " + j + ")");
	}

	@Override
	public String toString() {
		StringBuilder st = new StringBuilder();
		for (int i = 0; i < _vecinos.size(); i++) {
			st.append(i);
			st.append(", tiene como vecinos a: ");
			st.append(_vecinos.get(i));
			st.append("\n");
		}
		return st.toString();
	}

	@Override
	public boolean equals(Object obj) {
		if (!obj.getClass().getName().equals(this.getClass().getName())) {
			return false;
		}
		Grafo objGrafo = (Grafo) obj;
		if (!objGrafo._vecinos.equals(_vecinos)) {
			return false;
		}
		return true;
	}

}

package grafos;

import java.util.Set;
import java.util.TreeSet;

public class Prim {

	private Set<Integer> _verticesVisitados;
	private GrafoConPeso _AGM;
	private GrafoConPeso _grafoOriginal;

	public Prim(GrafoConPeso grafo) {
		_grafoOriginal = grafo;
		_verticesVisitados = new TreeSet<>();
		_AGM = new GrafoConPeso(grafo.tamano());
		generarAGM();
	}

	public GrafoConPeso getAGM() {
		return new GrafoConPeso(_AGM);
	}

	private void generarAGM() {
		if (!BFS.esConexo(_grafoOriginal)) {
			throw new IllegalArgumentException("No existe un AGM de un grafo disconexo.");
		}
		_verticesVisitados.add(0);

		for (int vertice = 0; vertice < _AGM.tamano(); vertice++) {
			Arista aristaMin = null;
			double pesoMin = Integer.MAX_VALUE;
			for (Arista arista : _grafoOriginal.getAristas()) {
				if (arista.getPeso() < pesoMin && unoContenidoYelOtroNo(arista.getV1(), arista.getV2())) {
					pesoMin = arista.getPeso();
					aristaMin = new Arista(arista.getV1(), arista.getV2(), pesoMin);
				}
			}
			if (aristaMin != null) {
				_AGM.agregarArista(aristaMin.getV1(), aristaMin.getV2(), pesoMin);
				_verticesVisitados.add(aristaMin.getV1());
				_verticesVisitados.add(aristaMin.getV2());
			}
		}
	}

	private boolean unoContenidoYelOtroNo(int i, int j) {
		return (_verticesVisitados.contains(i) && !_verticesVisitados.contains(j))
				|| (_verticesVisitados.contains(j) && !_verticesVisitados.contains(i));
	}
}
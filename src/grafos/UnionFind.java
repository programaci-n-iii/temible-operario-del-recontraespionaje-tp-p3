package grafos;

public class UnionFind {

	private int[] _A;

	public UnionFind(Grafo grafo) {
		_A = new int[grafo.tamano()];

		inicializarArreglo();
	}

	private void inicializarArreglo() {
		for (int i = 0; i < _A.length; i++)
			_A[i] = i;
	}

	public void union(int i, int j) {
		int raiz_i = raiz(i);
		int raiz_j = raiz(j);

		_A[raiz_i] = raiz_j;
	}

	public boolean find(int i, int j) {
		return raiz(i) == raiz(j);
	}

	public int raiz(int i) {
		while (_A[i] != i)
			i = _A[i];

		return i;
	}
}
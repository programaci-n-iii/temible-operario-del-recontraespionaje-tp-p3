package auxiliares;

import grafos.GrafoConPeso;
import grafos.Kruskal;
import grafos.Prim;

public class TiemposDeEjecucion {

	private Float _tiempoPrim;
	private Float _tiempoKruskal;

	public TiemposDeEjecucion(GrafoConPeso g) {
		_tiempoPrim = tomarTiempo(g, "Prim");
		_tiempoKruskal = tomarTiempo(g, "Kruskal");
	}

	@SuppressWarnings("unused")
	private Float tomarTiempo(GrafoConPeso g, String algoritmo) {
		long t_inicio, t_final;

		if (algoritmo.equals("Prim")) {
			t_inicio = System.currentTimeMillis();
			Prim agm = new Prim(g);
			t_final = System.currentTimeMillis();
		} else {
			t_inicio = System.currentTimeMillis();
			Kruskal agm = new Kruskal(g);
			t_final = System.currentTimeMillis();
		}

		return (float) (t_final - t_inicio);
	}

	@Override
	public String toString() {
		return "El algoritmo de Prim ha tomado " + _tiempoPrim / 1000.0 + " segundos (" + _tiempoPrim + " milisegundos"
				+ "). \n" + "El algoritmo de Kruskal " + _tiempoKruskal / 1000.0 + " segundos (" + _tiempoKruskal
				+ " milisegundos).";
	}
}
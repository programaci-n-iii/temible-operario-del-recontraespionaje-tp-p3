package auxiliares;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Map;
import java.util.TreeMap;

import com.google.gson.Gson;

import main.EncuentroDeEspias;

public class ManejoDeArchivos {

	public static final String IMG_GRAFO = "images/grafo.png", IMG_CANT_ESPIAS = "images/cantidadEspias.png",
			IMG_NOMBRES_ESPIAS = "images/nombresEspias.png", IMG_PAR_ESPIAS = "images/parDeEspias.png";

	private static final String JSON_ESPIAS_SERIE = "files/tresEspiasDeSuperAgente86",
			JSON_ESPIAS_ABC = "files/nueveEspiasAbecedario", IMG_ESPIAS_SERIE = "images/tresEspiasDeJSON.png",
			IMG_ESPIAS_ABC = "images/nueveEspiasDeJSON.png";

	public static Map<String, String> crearJSONsConIMGs() {
		Map<String, String> archivosConIMGs = new TreeMap<>();
		archivosConIMGs.put(JSON_ESPIAS_SERIE, IMG_ESPIAS_SERIE);
		archivosConIMGs.put(JSON_ESPIAS_ABC, IMG_ESPIAS_ABC);
		return archivosConIMGs;
	}

	public static EncuentroDeEspias leerEncuentroDeEspiasJSON(String archivo) {
		Gson gson = new Gson();
		EncuentroDeEspias ret = null;

		try {
			BufferedReader br = new BufferedReader(new FileReader(archivo));
			ret = gson.fromJson(br, EncuentroDeEspias.class);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return ret;
	}
}

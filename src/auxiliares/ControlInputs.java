package auxiliares;

import java.util.List;

import javax.swing.JOptionPane;

import main.EncuentroDeEspias;

public class ControlInputs {

	public final static List<String> PROBABILIDADES = List.of("0", "0.1", "0.2", "0.3", "0.4", "0.5", "0.6", "0.7",
			"0.8", "0.9", "1");

	public static boolean nombreEspiaPermitido(EncuentroDeEspias espias, String input) {
		if (input.isEmpty()) {
			JOptionPane.showMessageDialog(null, "No ingresa el nombre del espia.", "Nombre invalido", 0);
			return false;
		} else if (espias.getNombres().contains(input)) {
			JOptionPane.showMessageDialog(null, "Nombre de espia ya existente.", "Nombre invalido", 0);
			return false;
		}
		return true;
	}

	public static boolean cantidadEspiasPermitida(String input) {
		if (input.isEmpty()) {
			JOptionPane.showMessageDialog(null, "No ingresa la cantidad de los espias.", "Cantidad invalida", 0);
			return false;
		} else if (contieneAlMenosUnaLetra(input)) {
			JOptionPane.showMessageDialog(null, "No debe ingresar letras.", "Cantidad invalida", 0);
			return false;
		} else if (Integer.parseInt(input) <= 1) {
			JOptionPane.showMessageDialog(null, "La cantidad de los espias debe ser mayor a 1.", "Cantidad invalida",
					0);
			return false;
		}
		return true;
	}

	private static boolean contieneAlMenosUnaLetra(String input) {
		List<String> letras = List.of("a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "ñ", "o",
				"p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z");
		for (int i = 0; i < letras.size(); i++) {
			if (input.contains(letras.get(i))) {
				return true;
			}
		}
		return false;
	}

	public static void paresEspiasIguales() {
		JOptionPane.showMessageDialog(null, "El par de espias no pueden ser el mismo espia.", "Par de espias invalido",
				0);
	}

	public static void espiasAislados() {
		JOptionPane.showMessageDialog(null, "No puede haber espias que no tengan un 'camino' con los demas\n"
				+ "espias. Ingrese mas pares de espias.", "Hay espias que no pueden llegar a otros espias", 0);
	}

}

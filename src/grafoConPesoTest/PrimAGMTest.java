package grafoConPesoTest;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import grafos.GrafoConPeso;
import grafos.Prim;

public class PrimAGMTest {
	
	GrafoConPeso triangulo;
	GrafoConPeso aislado;
	Prim agmTriangulo;

	@Before
	public void init() {
		triangulo = new GrafoConPeso(3);
		triangulo.agregarArista(0, 1, 0.5);
		triangulo.agregarArista(0, 2, 0.2);
		triangulo.agregarArista(1, 2, 0.6);

		agmTriangulo = new Prim(triangulo);
		
	}

	@Test
	public void happyPathTest() {
		GrafoConPeso agm = new GrafoConPeso(3);
		agm.agregarArista(0, 1, 0.5);
		agm.agregarArista(0, 2, 0.2);

		assertEquals(agm, agmTriangulo.getAGM());
	}

	@Test(expected = IllegalArgumentException.class)
	public void grafoNoConexo() {
		GrafoConPeso g = new GrafoConPeso(5);
		Prim agm = new Prim(g);
		agm.getAGM();
	}
	
	@Test
	public void encontrarAntesPesoMaximoTest() {
		GrafoConPeso grafo = new GrafoConPeso(3);
		grafo.agregarArista(0, 1, 0.2);
		grafo.agregarArista(0, 2, 0.3);
		grafo.agregarArista(1, 2, 0.7);
		
		GrafoConPeso grafoEsperado = new GrafoConPeso(3);
		grafoEsperado.agregarArista(0, 1, 0.2);
		grafoEsperado.agregarArista(0, 2, 0.3);
		
		Prim agm = new Prim(grafo);

		assertEquals(grafoEsperado, agm.getAGM());
	}
	
	@Test
	public void unicaAristaTest() {
		GrafoConPeso grafo = new GrafoConPeso(2);
		grafo.agregarArista(0, 1, 2.0);
		
		GrafoConPeso grafoEsperado = new GrafoConPeso(2);
		grafoEsperado.agregarArista(0, 1, 2.0);
		
		Prim agm = new Prim(grafo);
		
		assertEquals(grafoEsperado, agm.getAGM());
	}
	
	@Test
	public void vacioTest() {
		GrafoConPeso grafo = new GrafoConPeso(0);
		Prim agm = new Prim(grafo);
		assertEquals(new GrafoConPeso(0), agm.getAGM());
	}
}

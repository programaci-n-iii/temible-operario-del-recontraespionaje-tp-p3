package grafoConPesoTest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

import grafos.Arista;
import grafos.GrafoConPeso;

public class AristasTest {
	
	GrafoConPeso grafo;

	@Before
	public void init() {
		grafo = new GrafoConPeso(3);
	}
	
	@Test
	public void aristaExistenteTest() {
		grafo.agregarArista(0, 1, 0.5);
		
		Arista agregada = new Arista(0, 1, 0.5);
		assertTrue(grafo.getAristas().contains(agregada));
	}
	
	@Test
	public void aristaInexistenteTest() {
		Arista arista = new Arista(0, 1, 0.5);
		assertFalse(grafo.getAristas().contains(arista));
	}
	
	@Test
	public void sinAristasTest() {
		assertTrue(grafo.getAristas().isEmpty());
	}
	
	@Test
	public void aristaOpuestaTest() {
		grafo.agregarArista(0, 2, 0.2);
		
		Arista agregada = new Arista(2, 0, 0.2);
		assertTrue(grafo.getAristas().contains(agregada));
	}

	@Test
	public void agregarAristaDosVecesTest() {
		grafo.agregarArista(2, 1, 0.1);
		grafo.agregarArista(2, 1, 0.1);
		
		assertEquals(1, grafo.getAristas().size());
	}
	
	@Test
	public void agregarAristaYsuOpuestaTest() {
		grafo.agregarArista(1, 2, 0.3);
		grafo.agregarArista(1, 2, 0.3);
		
		assertEquals(1, grafo.getAristas().size());
	}

}

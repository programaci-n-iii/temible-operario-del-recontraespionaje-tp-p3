package grafoConPesoTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import grafos.Grafo;
import grafos.UnionFind;

public class UnionFindTest {

	Grafo g = new Grafo(10);
	UnionFind A = new UnionFind(g);

	@Before
	public void init() {
		g.agregarArista(1, 2);
		A.union(1, 2);
		g.agregarArista(2, 3);
		A.union(2, 3);
		g.agregarArista(5, 3);
		A.union(5, 3);
		// componente conexa g1

		g.agregarArista(8, 7);
		A.union(8, 7);
		g.agregarArista(9, 7);
		A.union(9, 7);
		// componente conexa g2

		// componente g3 = vertice 0; g4 = vertice 4; g5 = vertice 6
	}

	@Test
	public void raiz() {
		assertEquals(A.raiz(1), A.raiz(2));
		// comparten la raiz 3
	}

	@Test
	public void distintasComponentes() {
		assertFalse(A.raiz(1) == A.raiz(7));
		assertFalse(A.raiz(0) == A.raiz(3));
		assertFalse(A.raiz(0) == A.raiz(4) || A.raiz(0) == A.raiz(6));
	}

	@Test
	public void find() {
		assertTrue(A.find(1, 2));
		assertFalse(A.find(4, 7));
	}

	@Test
	public void union() {
		A.union(0, 1);
		assertEquals(A.raiz(0), A.raiz(1));

		assertTrue(A.find(0, 1));
		assertTrue(A.find(0, 2));
		assertTrue(A.find(0, 5));

		assertEquals(A.raiz(0), A.raiz(3));
		assertEquals(A.raiz(0), A.raiz(2));
		assertEquals(A.raiz(0), A.raiz(5));

		assertFalse(A.raiz(0) == A.raiz(8));
	}
}
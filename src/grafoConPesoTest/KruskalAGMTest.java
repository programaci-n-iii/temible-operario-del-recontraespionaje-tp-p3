package grafoConPesoTest;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import grafos.GrafoConPeso;
import grafos.Kruskal;

public class KruskalAGMTest {
	
	GrafoConPeso triangulo;
	GrafoConPeso aislado;
	Kruskal agmTriangulo;;

	@Before
	public void init() {
		triangulo = new GrafoConPeso(3);
		triangulo.agregarArista(0, 1, 0.5);
		triangulo.agregarArista(0, 2, 0.2);
		triangulo.agregarArista(1, 2, 0.6);

		agmTriangulo = new Kruskal(triangulo);
		
	}

	@Test
	public void happyPathTest() {
		GrafoConPeso agm = new GrafoConPeso(3);
		agm.agregarArista(0, 1, 0.5);
		agm.agregarArista(0, 2, 0.2);

		assertEquals(agm, agmTriangulo.getAGM());
	}

	@Test(expected = IllegalArgumentException.class)
	public void grafoNoConexo() {
		GrafoConPeso g = new GrafoConPeso(5);
		Kruskal agm = new Kruskal(g);
		agm.getAGM();
	}
	
	@Test
	public void encontrarAntesPesoMaximoTest() {
		GrafoConPeso grafo = new GrafoConPeso(3);
		grafo.agregarArista(0, 1, 0.2);
		grafo.agregarArista(0, 2, 0.3);
		grafo.agregarArista(1, 2, 0.7);
		
		GrafoConPeso grafoEsperado = new GrafoConPeso(3);
		grafoEsperado.agregarArista(0, 1, 0.2);
		grafoEsperado.agregarArista(0, 2, 0.3);
		
		Kruskal agm = new Kruskal(grafo);

		assertEquals(grafoEsperado, agm.getAGM());
	}
	
	@Test
	public void bordeTest() {
		GrafoConPeso grafo = new GrafoConPeso(2);
		grafo.agregarArista(0, 1, 2.0);
		
		GrafoConPeso grafoEsperado = new GrafoConPeso(2);
		grafoEsperado.agregarArista(0, 1, 2.0);
		
		Kruskal agm = new Kruskal(grafo);
		
		assertEquals(grafoEsperado, agm.getAGM());
	}
	
	@Test
	public void vacioTest() {
		GrafoConPeso grafo = new GrafoConPeso(0);
		Kruskal agm = new Kruskal(grafo);
		assertEquals(new GrafoConPeso(0), agm.getAGM());
	}
}

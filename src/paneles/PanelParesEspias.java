package paneles;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JPanel;
import main.EncuentroDeEspias;
import java.awt.Font;
import java.awt.Cursor;
import auxiliares.ControlInputs;
import auxiliares.ManejoDeArchivos;

import javax.swing.JLabel;

@SuppressWarnings("serial")
public class PanelParesEspias extends JPanel {

	public PanelParesEspias(JFrame frame, EncuentroDeEspias espias) {
		setSize(640, 480);
		setLayout(null);

		JComboBox<String> seleccionEspias1 = new JComboBox<>();
		seleccionEspias1.setFont(new Font("Dialog", Font.PLAIN, 15));
		seleccionEspias1.setBounds(98, 95, 125, 30);
		add(seleccionEspias1);

		JComboBox<String> seleccionEspias2 = new JComboBox<>();
		seleccionEspias2.setFont(new Font("Dialog", Font.PLAIN, 15));
		seleccionEspias2.setBounds(401, 95, 125, 30);
		add(seleccionEspias2);

		seleccionEspias1.setModel(new DefaultComboBoxModel<>(new String[] {}));
		seleccionEspias2.setModel(new DefaultComboBoxModel<>(new String[] {}));

		agregarNombresAlcomBox(espias, seleccionEspias1, seleccionEspias2);

		JComboBox<String> seleccionProbabilidad = new JComboBox<>();
		seleccionProbabilidad.setFont(new Font("Dialog", Font.PLAIN, 15));
		seleccionProbabilidad.setBounds(468, 136, 70, 30);
		add(seleccionProbabilidad);
		agregarProbabilidades(seleccionProbabilidad);

		JButton botonAgregarPar = new JButton("Agregar par");
		botonAgregarPar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonAgregarPar.setFont(new Font("Dialog", Font.PLAIN, 15));
		botonAgregarPar.setBounds(233, 169, 125, 30);
		botonAgregarPar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				String espiaElegido1 = espias.getNombres().get(seleccionEspias1.getSelectedIndex());
				String espiaElegido2 = espias.getNombres().get(seleccionEspias2.getSelectedIndex());
				double probabilidadElegida = Double.parseDouble((String) seleccionProbabilidad.getSelectedItem());

				try {
					espias.agregarParesEspias(espiaElegido1, espiaElegido2, probabilidadElegida);
				} catch (Exception ex) {
					ControlInputs.paresEspiasIguales();
					return;
				}
			}

		});
		add(botonAgregarPar);

		JButton botonFinalizar = new JButton("Finalizar");
		botonFinalizar.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				try {
					espias.minimizarEncuentroRiesgoso();
				} catch (Exception ex) {
					ControlInputs.espiasAislados();
					return;
				}

				PanelParesEspias.this.setVisible(false);
				PanelEncuentros panelresultado = new PanelEncuentros(frame, espias);
				frame.getContentPane().add(panelresultado);
			}

		});
		botonFinalizar.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonFinalizar.setFont(new Font("Dialog", Font.PLAIN, 15));
		botonFinalizar.setBounds(480, 390, 125, 30);
		add(botonFinalizar);

		JLabel textoIngresarProbabilidad = new JLabel("Seleccione probabilidad de intercepcion al par de espias:");
		textoIngresarProbabilidad.setFont(new Font("Dialog", Font.PLAIN, 15));
		textoIngresarProbabilidad.setBounds(90, 138, 418, 20);
		add(textoIngresarProbabilidad);

		JLabel textoIndicarEncuentro = new JLabel("se puede encontrar con");
		textoIndicarEncuentro.setFont(new Font("Dialog", Font.PLAIN, 15));
		textoIndicarEncuentro.setBounds(233, 99, 158, 26);
		add(textoIndicarEncuentro);

		JLabel textoElegirPar = new JLabel("Elija los pares de espias que se pueden encontrar:");
		textoElegirPar.setFont(new Font("Dialog", Font.PLAIN, 15));
		textoElegirPar.setBounds(137, 61, 333, 23);
		add(textoElegirPar);

		JLabel imgParEspias = new JLabel(new ImageIcon(ManejoDeArchivos.IMG_PAR_ESPIAS));
		imgParEspias.setBounds(100, 210, 363, 207);
		add(imgParEspias);

	}

	private void agregarNombresAlcomBox(EncuentroDeEspias espias, JComboBox<String> izq, JComboBox<String> der) {
		for (int i = 0; i < espias.cantidad(); i++) {
			izq.addItem(espias.getNombres().get(i));
			der.addItem(espias.getNombres().get(i));
		}
	}

	private void agregarProbabilidades(JComboBox<String> comboBoxProbabilidad) {
		for (String p : ControlInputs.PROBABILIDADES) {
			comboBoxProbabilidad.addItem(p);
		}
	}
}

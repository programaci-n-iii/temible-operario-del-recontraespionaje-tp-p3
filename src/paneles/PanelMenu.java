package paneles;

import java.awt.Cursor;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import auxiliares.ManejoDeArchivos;

@SuppressWarnings("serial")
public class PanelMenu extends JPanel {

	public PanelMenu(JFrame frame) {
		setSize(640, 480);
		setLayout(null);

		JButton botonIngresarDatos = new JButton("Ingresar datos manualmente");
		botonIngresarDatos.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonIngresarDatos.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				PanelMenu.this.setVisible(false);

				PanelCantidadEspias panelCantidadEspias = new PanelCantidadEspias(frame);
				frame.getContentPane().add(panelCantidadEspias);
			}

		});
		botonIngresarDatos.setFont(new Font("Dialog", Font.PLAIN, 15));
		botonIngresarDatos.setBounds(33, 66, 227, 40);
		add(botonIngresarDatos);

		JButton botonLeerArchivo = new JButton("Leer archivo con datos");
		botonLeerArchivo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonLeerArchivo.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				PanelMenu.this.setVisible(false);

				PanelArchivos panelArchivos = new PanelArchivos(frame);
				frame.getContentPane().add(panelArchivos);
			}

		});
		botonLeerArchivo.setFont(new Font("Dialog", Font.PLAIN, 15));
		botonLeerArchivo.setBounds(33, 117, 227, 40);
		add(botonLeerArchivo);

		JButton botonSalir = new JButton("Salir del programa");
		botonSalir.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				frame.dispose();
			}

		});
		botonSalir.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonSalir.setFont(new Font("Dialog", Font.PLAIN, 15));
		botonSalir.setBounds(33, 168, 227, 40);
		add(botonSalir);

		JLabel imagenGrafo = new JLabel(new ImageIcon(ManejoDeArchivos.IMG_GRAFO));
		imagenGrafo.setBounds(255, 66, 350, 350);
		add(imagenGrafo);
	}
}

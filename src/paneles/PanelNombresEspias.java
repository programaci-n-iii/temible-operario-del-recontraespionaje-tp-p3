package paneles;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import auxiliares.ControlInputs;
import auxiliares.ManejoDeArchivos;
import main.EncuentroDeEspias;
import java.awt.Cursor;

@SuppressWarnings("serial")
public class PanelNombresEspias extends JPanel {

	public PanelNombresEspias(JFrame frame, EncuentroDeEspias espias) {
		setSize(640, 480);
		setLayout(null);

		JLabel textoPedirNombre = new JLabel("Escriba el nombre del espia:");
		textoPedirNombre.setFont(new Font("Dialog", Font.PLAIN, 15));
		textoPedirNombre.setBounds(55, 60, 195, 23);
		add(textoPedirNombre);

		JLabel textoEspiasAgregados = new JLabel("0/" + espias.getGrafo().tamano() + " espias agregados");
		textoEspiasAgregados.setFont(new Font("Dialog", Font.PLAIN, 15));
		textoEspiasAgregados.setBounds(387, 60, 150, 22);
		add(textoEspiasAgregados);

		JTextField inputNombre = new JTextField();
		inputNombre.setFont(new Font("Dialog", Font.PLAIN, 15));
		inputNombre.setBounds(252, 60, 125, 30);
		add(inputNombre);
		inputNombre.setColumns(10);
		add(inputNombre);

		JButton botonSiguiente = new JButton("Siguiente");
		botonSiguiente.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonSiguiente.setFont(new Font("Dialog", Font.PLAIN, 15));
		botonSiguiente.setEnabled(false);
		botonSiguiente.setBounds(480, 390, 125, 30);
		botonSiguiente.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				PanelNombresEspias.this.setVisible(false);
				PanelParesEspias panelProbabilidad = new PanelParesEspias(frame, espias);
				frame.getContentPane().add(panelProbabilidad);
			}

		});
		add(botonSiguiente);

		JButton botonAgregarEspia = new JButton("Agregar espia");
		botonAgregarEspia.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonAgregarEspia.setFont(new Font("Dialog", Font.PLAIN, 15));
		botonAgregarEspia.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if (!ControlInputs.nombreEspiaPermitido(espias, inputNombre.getText()))
					return;

				espias.agregarEspia(inputNombre.getText());
				actualizarInputYbotones(espias, inputNombre, botonSiguiente, botonAgregarEspia);
				textoEspiasAgregados
						.setText(espias.cantidad() + "/" + espias.getGrafo().tamano() + " espias agregados");
			}

		});
		botonAgregarEspia.setBounds(252, 390, 125, 30);
		add(botonAgregarEspia);

		JLabel imgNombresEspias = new JLabel(new ImageIcon(ManejoDeArchivos.IMG_NOMBRES_ESPIAS));
		imgNombresEspias.setBounds(70, 105, 485, 275);
		add(imgNombresEspias);

	}

	private void actualizarInputYbotones(EncuentroDeEspias espias, JTextField inputNombre, JButton botonSiguiente,
			JButton botonAgregarEspia) {
		inputNombre.setText(null);
		if (espias.cantidad() == espias.getGrafo().tamano()) {
			inputNombre.setEnabled(false);
			botonAgregarEspia.setEnabled(false);
			botonSiguiente.setEnabled(true);
		}
	}
}

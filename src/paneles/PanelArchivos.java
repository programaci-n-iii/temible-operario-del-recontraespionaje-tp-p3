package paneles;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Map;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import auxiliares.ManejoDeArchivos;
import main.EncuentroDeEspias;
import java.awt.Cursor;

@SuppressWarnings("serial")
public class PanelArchivos extends JPanel {

	public PanelArchivos(JFrame frame) {
		setSize(640, 480);
		setLayout(null);

		JLabel textoSeleccionarArchivo = new JLabel(
				"Seleccione un archivo para leer y ver que espias se deberían encontrar:");
		textoSeleccionarArchivo.setFont(new Font("Dialog", Font.PLAIN, 15));
		textoSeleccionarArchivo.setBounds(73, 45, 484, 38);
		add(textoSeleccionarArchivo);

		JLabel textoEspias = new JLabel();
		textoEspias.setFont(new Font("Dialog", Font.PLAIN, 15));
		textoEspias.setBounds(73, 133, 484, 38);
		add(textoEspias);

		JComboBox<String> seleccionArchivos = new JComboBox<>();
		seleccionArchivos.setFont(new Font("Dialog", Font.PLAIN, 15));
		seleccionArchivos.setBounds(98, 99, 276, 28);
		add(seleccionArchivos);

		seleccionArchivos.setModel(new DefaultComboBoxModel<>(new String[] {}));
		Map<String, String> jsonsConImgs = ManejoDeArchivos.crearJSONsConIMGs();
		for (String json : jsonsConImgs.keySet()) {
			seleccionArchivos.addItem(json);
		}

		JLabel imagenEspias = new JLabel(new ImageIcon(jsonsConImgs.get(seleccionArchivos.getSelectedItem())));
		imagenEspias.setBounds(70, 145, 485, 275);
		add(imagenEspias);

		seleccionArchivos.addActionListener(new ActionListener() {

			@Override
			public void actionPerformed(ActionEvent e) {
				cambiarImagenAlElegirArchivo(imagenEspias, seleccionArchivos, jsonsConImgs);
			}

		});

		JButton botonLeerArchivo = new JButton("Leer archivo");
		botonLeerArchivo.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonLeerArchivo.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				PanelArchivos.this.setVisible(false);

				String jsonSeleccionado = (String) seleccionArchivos.getSelectedItem();
				EncuentroDeEspias espias = ManejoDeArchivos.leerEncuentroDeEspiasJSON(jsonSeleccionado);

				espias.minimizarEncuentroRiesgoso();
				PanelEncuentros panelresultado = new PanelEncuentros(frame, espias);
				frame.getContentPane().add(panelresultado);
			}

		});
		botonLeerArchivo.setFont(new Font("Dialog", Font.PLAIN, 15));
		botonLeerArchivo.setBounds(384, 94, 146, 38);
		add(botonLeerArchivo);
	}

	private void cambiarImagenAlElegirArchivo(JLabel imagenEspias, JComboBox<String> seleccionArchivos,
			Map<String, String> jsonsConImgs) {
		for (String json : jsonsConImgs.keySet()) {
			if (seleccionArchivos.getSelectedItem().equals(json)) {
				imagenEspias.setIcon(new ImageIcon(jsonsConImgs.get(json)));
			}
		}
	}
}

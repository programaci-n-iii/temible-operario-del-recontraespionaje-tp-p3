package paneles;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import auxiliares.ControlInputs;
import auxiliares.ManejoDeArchivos;
import main.EncuentroDeEspias;

import java.awt.Font;
import java.awt.Cursor;

@SuppressWarnings("serial")
public class PanelCantidadEspias extends JPanel {

	private JTextField _inputCantidad;

	public PanelCantidadEspias(JFrame frame) {
		setSize(640, 480);
		setLayout(null);

		JLabel TextoIngresarCantidad = new JLabel("Ingrese la cantidad de espias que desee:");
		TextoIngresarCantidad.setFont(new Font("Dialog", Font.PLAIN, 15));
		TextoIngresarCantidad.setBounds(135, 60, 270, 27);
		add(TextoIngresarCantidad);

		_inputCantidad = new JTextField();
		_inputCantidad.setFont(new Font("Dialog", Font.PLAIN, 15));
		_inputCantidad.setBounds(415, 60, 40, 25);
		add(_inputCantidad);
		_inputCantidad.setColumns(10);

		JButton botonSiguiente = new JButton("Siguiente");
		botonSiguiente.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		botonSiguiente.setFont(new Font("Dialog", Font.PLAIN, 15));
		botonSiguiente.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				if (!ControlInputs.cantidadEspiasPermitida(_inputCantidad.getText())) {
					_inputCantidad.setText(null);
					return;
				}
				EncuentroDeEspias espias = new EncuentroDeEspias(Integer.parseInt(_inputCantidad.getText()));

				PanelCantidadEspias.this.setVisible(false);
				PanelNombresEspias panelNombresEspias = new PanelNombresEspias(frame, espias);
				frame.getContentPane().add(panelNombresEspias);
			}

		});
		botonSiguiente.setBounds(480, 390, 125, 30);
		add(botonSiguiente);

		JLabel imgCantEspias = new JLabel(new ImageIcon(ManejoDeArchivos.IMG_CANT_ESPIAS));
		imgCantEspias.setBounds(70, 105, 485, 275);
		add(imgCantEspias);
	}
}

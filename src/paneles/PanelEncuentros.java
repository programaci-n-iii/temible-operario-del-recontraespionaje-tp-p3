package paneles;

import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import auxiliares.TiemposDeEjecucion;
import main.EncuentroDeEspias;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Cursor;

@SuppressWarnings("serial")
public class PanelEncuentros extends JPanel {

	private JTextArea _areaTextoEncuentros;

	public PanelEncuentros(JFrame frame, EncuentroDeEspias espias) {
		setSize(640, 480);
		setLayout(null);

		JLabel textoIndicarEncuentros = new JLabel("Encuentros entre:");
		textoIndicarEncuentros.setFont(new Font("Dialog", Font.BOLD, 15));
		textoIndicarEncuentros.setBounds(243, 66, 198, 23);
		add(textoIndicarEncuentros);

		_areaTextoEncuentros = new JTextArea();
		_areaTextoEncuentros.setFont(new Font("Dialog", Font.PLAIN, 15));
		_areaTextoEncuentros.setEditable(false);
		_areaTextoEncuentros.setBounds(25, 100, 575, 200);
		_areaTextoEncuentros
				.setText(espias.mejorEncuentroEspias() + "\n" + "Orden segun BFS: \n" + espias.encuentroEspiasBFS());
		add(_areaTextoEncuentros);

		JScrollPane scroll = new JScrollPane(_areaTextoEncuentros, ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS,
				ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
		scroll.setBounds(25, 100, 575, 200);
		add(scroll);

		JTextArea textAreaTiemposEj = new JTextArea();
		textAreaTiemposEj.setEditable(false);
		textAreaTiemposEj.setFont(new Font("Dialog", Font.PLAIN, 15));
		textAreaTiemposEj.setBounds(25, 361, 508, 62);
		add(textAreaTiemposEj);

		JButton btnTiemposEj = new JButton("Tiempos Prim/Kruskal ");
		btnTiemposEj.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		btnTiemposEj.setFont(new Font("Dialog", Font.PLAIN, 15));
		btnTiemposEj.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				TiemposDeEjecucion tiempos = new TiemposDeEjecucion(espias.getGrafo());
				textAreaTiemposEj.setText(tiempos.toString());
			}

		});
		btnTiemposEj.setBounds(25, 321, 198, 29);
		add(btnTiemposEj);
	}
}